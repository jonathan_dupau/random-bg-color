const body = document.querySelector('body');
const color = document.getElementById('color');

const randomNumber = () => {
    var firstRandomNumber = Math.floor(Math.random() * 255);
    var secondRandomNumber = Math.floor(Math.random() * 255);
    var thirdRandomNumber = Math.floor(Math.random() * 255);
    body.style.backgroundColor="rgb(" + firstRandomNumber + "," + secondRandomNumber + "," + thirdRandomNumber + ")";
    color.innerHTML = `rgb(${firstRandomNumber}, ${secondRandomNumber}, ${thirdRandomNumber})`
}
setInterval(randomNumber, 1000);



